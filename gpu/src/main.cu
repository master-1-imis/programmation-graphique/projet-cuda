#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <chrono>
#include <string>
#include <opencv2/opencv.hpp>



/******************************************************************************
*	CONSTANTS                                                                 *
******************************************************************************/
const unsigned int MODE_RGB  = 3;
const unsigned int MODE_RGBA = 4;

const double host_matrix_trans[9] = {
	0.0, -0.1,  0.0,
	0.0,  0.6, -0.1,
	1.0,  0.0,  0.0
};



/******************************************************************************
*	FUNCTION                                                                  *
******************************************************************************/
__global__ void Traitement(const unsigned char* device_data_in, unsigned char* device_data_out, unsigned int width, unsigned int height, unsigned int mode, const double* matrix_trans) {

	// Coordonnées 2D du thread courant
	auto j = blockIdx.x * blockDim.x + threadIdx.x;
	auto i = blockIdx.y * blockDim.y + threadIdx.y;

	// faire les calcul que si les threads sont sur un pixel de l'image
	if (j < width && i < height) {

		unsigned int pos_global = i * width * mode + j*mode;

		unsigned char comp_blue  = device_data_in[pos_global];
		unsigned char comp_green = device_data_in[pos_global+1];
		unsigned char comp_red   = device_data_in[pos_global+2];
		unsigned char comp_alpha = mode == MODE_RGBA ? device_data_in[pos_global+3] : 255;

		// de pas changer les pixels du bord de l'image
		if (i == 0 || i == height - 1 || j == 0 || j == width - 1) {
			device_data_out[pos_global]   = comp_blue;
			device_data_out[pos_global+1] = comp_green;
			device_data_out[pos_global+2] = comp_red;
			if (mode == MODE_RGBA) { device_data_out[pos_global+3] = comp_alpha; }
		}
		else {
			double coef = 0.0;
			unsigned int pos_local = 0;

			double blue  = 0.0;
			double green = 0.0;
			double red   = 0.0;

			// calcul de la nouvelle couleur du pixel courant en fonction des voisins
			for (int a = -1; a < 2; a++) {
				for (int b = -1; b < 2; b++) {
					coef = matrix_trans[(1+a)*mode + (1+b)];
					pos_local = (i+a) * width * mode + (j+b) * mode;

					blue  += coef * device_data_in[pos_local];
					green += coef * device_data_in[pos_local+1];
					red   += coef * device_data_in[pos_local+2];
				}
			}

			// normaliser les valeurs pour rester dans la borne [0:255]
			blue  = blue  < 0 ? 0 : blue  > 255 ? 255 : blue;
			green = green < 0 ? 0 : green > 255 ? 255 : green;
			red   = red   < 0 ? 0 : red   > 255 ? 255 : red;

			// sauvegarder le nouveau pixel
			device_data_out[pos_global]   = (unsigned char)blue;
			device_data_out[pos_global+1] = (unsigned char)green;
			device_data_out[pos_global+2] = (unsigned char)red;
			if (mode == MODE_RGBA) { device_data_out[pos_global+3] = comp_alpha; }
		}
	}
}




/******************************************************************************
*	MAIN                                                                      *
******************************************************************************/
int main(int argc, char* argv[]) {

	if (argc != 4) {
		printf("Erreur de paramètre: main-cu [nom fichier] [block x] [block y]");
		return EXIT_FAILURE;
	}

	// récupération du chemin d'accès à l'image d'entrée (fournie en paramètre)
	char* filepath_in = realpath(argv[1], NULL);
	if (filepath_in == NULL) {
		printf("Erreur de fichier: cette image n'existe pas ou n'est pas accésible.");
		return EXIT_FAILURE;
	}
	int block_x = atoi(argv[2]);
	int block_y = atoi(argv[3]);

	// définir le fichier de sortie
	std::string filepath_out(filepath_in);
	int index = filepath_out.find("img/in");
	filepath_out.replace(index, 6, "img/out");
	index = filepath_out.find_last_of(".");
	filepath_out.replace(index, 1, ".out.");

	// création des images d'entrée
	cv::Mat img_in  = cv::imread(filepath_in, cv::IMREAD_UNCHANGED);

	// définition des dimentions de l'image et du mode
	const unsigned int width = img_in.cols;
	const unsigned int height = img_in.rows;
	const unsigned int mode = img_in.elemSize();
	const unsigned int size = width * height * mode;

	if (width == 0 || height == 0) { return EXIT_FAILURE; }

	// std::cout << "Input:  " << filepath_in << std::endl;
	// std::cout << "Output: " << filepath_out << std::endl;
	//if (mode == MODE_RGB) {
	//	std::cout << "Mode:   RGB" << std::endl;
	//}
	//else {
	//	std::cout << "Mode:   RGBA" << std::endl;
	//}
	// std::cout << "Width:  " << width << std::endl;
	// std::cout << "Height: " << height << std::endl;

	// récupérer les données de l'image d'entrée
	unsigned char* host_data_in = img_in.data;
	unsigned char* device_data_in = nullptr;

	std::vector<unsigned char> host_data_out(size);
	unsigned char* device_data_out = nullptr;

	double* device_matrix_trans = nullptr;

	// création de l'image de sortie
	cv::Mat img_out(height, width, img_in.type(), host_data_out.data());

	// allocation de mémoire pour les données des images pour le GPU
	cudaMalloc(&device_data_in, size * sizeof(unsigned char));
	cudaMalloc(&device_data_out, size * sizeof(unsigned char));
	cudaMalloc(&device_matrix_trans, 9 * sizeof(double));

	// copier les données de "host" vers "device"
	cudaMemcpy(device_data_in, host_data_in, size * sizeof(unsigned char), cudaMemcpyHostToDevice);
	cudaMemcpy(device_matrix_trans, host_matrix_trans, 9 * sizeof(double), cudaMemcpyHostToDevice);


	// initialisation de la taille des blocs et de la grille
	dim3 block(block_x, block_y);
	dim3 grid((width-1)/block.x+1, (height-1)/block.y+1);

	//printf("threads: x=%i, y=%i\n", block.x, block.y);
	//printf("blocks: x=%i, y=%i\n", grid.x, grid.y);

	// mise en place du chrono
	cudaEvent_t start, stop;
        cudaEventCreate( &start );
        cudaEventCreate( &stop );

	// démarrage du chrono
        cudaEventRecord( start );

	// traitement
	Traitement<<<grid, block>>>(device_data_in, device_data_out, width, height, mode, device_matrix_trans);

	// arrêt du chrono
	cudaEventRecord( stop );
	cudaEventSynchronize( stop );

	// calcul le temps d'exécution et l'affiche
	float duration = 0.0f;
	cudaEventElapsedTime( &duration, start, stop );
	printf("Block (%d, %d) -> time: %f ms\n", block_x, block_y, duration);

	// récupération du code erreur du kernel en cas de plantage.
	cudaDeviceSynchronize();

	// copier les données de "device" vers "host"
	cudaMemcpy(host_data_out.data(), device_data_out, size * sizeof(unsigned char), cudaMemcpyDeviceToHost);

	// sauvegarder l'image de sortie
	cv::imwrite(filepath_out, img_out);

	// désallocation
	img_in.deallocate();
	img_out.deallocate();
	host_data_out.clear();
	cudaFree(device_data_in);
	cudaFree(device_data_out);
	cudaFree(device_matrix_trans);

	return EXIT_SUCCESS;
}
